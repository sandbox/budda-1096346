<?php

function quicktweet_settings() {

  $form['quicktweet_globaluser'] = array(
    '#type' => 'textfield', 
    '#title' => t('Global twitter user name'), 
    '#default_value' => variable_get('quicktweet_globaluser', ''), 
    '#size' => 60, 
    '#maxlength' => 128, 
  );
  
  $form['quicktweet']['quicktweet_header'] = array(
    '#type' => 'textarea', 
    '#title' => t('Header Message'), 
    '#default_value' => variable_get('quicktweet_header', ''), 
  );
  
  if (module_exists('token')) {
    $form['quicktweet']['token_help'] = array(
      '#title' => t('Replacement patterns'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
//      '#description' => t('Prefer raw-text replacements for text to avoid problems with HTML entities!'),
      );

    $form['quicktweet']['token_help']['help'] = array(
      '#value' => theme('token_help', 'user'),
    );
  }
  

    
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
};

function quicktweet_settings_submit($form, &$form_state) {
  variable_set('quicktweet_globaluser', $form_state['values']['quicktweet_globaluser']);
  variable_set('quicktweet_header', $form_state['values']['quicktweet_header']);
}